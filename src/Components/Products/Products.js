import { useContext } from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./Products.scss";

import ProductCard from "../ProductCard/ProductCard";
import ModalCart from '../Modal/ModalCart/ModalCart';
import Empty from "../Empty/Empty";
import { ProductsViewContext } from "../../Components/AppRoutes/AppRoutes";

const Products = ({ category,products }) => {
    const productsVieww = useContext(ProductsViewContext);
    
    return (
        <>
            {products.length === 0 && <Empty />}
            {products.length > 0 && <div className={cn("products",productsVieww?'cards':'list')}>
                {products.map((product)=><ProductCard
                key={product.id} 
                product={product}
                category={category}></ProductCard>)}
            </div>}
            <ModalCart />
        </>
    )
}

Products.propTypes = {
    category: PropTypes.string,
    products: PropTypes.array
  };

export default Products;