import {useSelector} from "react-redux";

import Icon from "../../Icon/Icon";

const IconCart = () => {
    const cartProducts = useSelector(state => state.main.cartProducts);

    return (
        <div className="header__cart-wrapper icon">
            <Icon src="/images/icons/cart.png" alt="cart" width="24px" height="24px"/>
            <span>{cartProducts.length}</span>
        </div>
    )
}

export default IconCart;