import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { useContext } from "react";
import { setCurrentCard, toggleFavorites, toggleModal } from "../../store/mainSlice";
import PropTypes from "prop-types";
import cn from "classnames";

import "./ProductCard.scss";

import ColorProductCard from "./ComponentsProductCard/ColorProductCard";
import ImageProductCard from "./ComponentsProductCard/ImageProductCard";
import PriceProductCard from "./ComponentsProductCard/PriceProductCard";
import TitleProductCard from "./ComponentsProductCard/TitleProductCard";
import { ReactComponent as Heart } from "../Svg/heart.svg";
import { ReactComponent as Cart } from "../Svg/cart.svg";
import { ReactComponent as Check} from "../Svg/check_circle.svg";
import { ProductsViewContext } from "../../Components/AppRoutes/AppRoutes";

const ProductCard = ({product, category, optionalClassNames}) => {
    const{src, title, color, price} = product;
    const favorites = useSelector(state => state.main.favorites);
    const cartProducts = useSelector(state => state.main.cartProducts);
    const dispatch = useDispatch();
    const productsVieww = useContext(ProductsViewContext);

    const addActiveClass = () => {
        const arrId = favorites.map(item=>item.id);
        if(arrId.includes(product.id)){
            return "active";
        }
    }

    const addActiveClassCart = () => {
        const arrId = cartProducts.map(item=>item.id);
        if(arrId.includes(product.id)){
            return "active-cart";
        }
    }

    return (
        <div className={cn("product-card",productsVieww?'card':'item')}>
            <Link className="product-card__link"to={`/catalog/${category}/${product.id}`}>
                <ImageProductCard src={src} optionalClassNames="product-card__image"/>
                <TitleProductCard optionalClassNames="product-card__title">{title}</TitleProductCard>
                <ColorProductCard color={color}/>
            </Link>  
            <div className="product-card__wrapper row">
                <PriceProductCard price={price}/>
                <div className="product-card__cart-favorites">
                    <div    
                        className={cn("product-card__heart",addActiveClass())} 
                        id={product.id} 
                        onClick={()=>dispatch(toggleFavorites(product))}>
                        <Heart />
                    </div>
                    <div 
                        className={cn("product-card__cart",addActiveClassCart())}
                        onClick={()=>{
                                dispatch(toggleModal("modalCart"));
                                dispatch(setCurrentCard(product));
                        }}>
                        <Cart />
                        <div className={cn("product-card__cart-check",addActiveClassCart())}>
                            <Check />
                        </div>
                    </div>
                </div>
            </div>          
        </div>
    )
}

ProductCard.propTypes = {
    product: PropTypes.object,
    category: PropTypes.string, 
  };

export default ProductCard;