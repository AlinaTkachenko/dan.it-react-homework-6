import { render, screen, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from 'redux-mock-store';

import { toggleCartProduct, toggleModal } from '../../../store/mainSlice';
import ModalText from "./ModalText";

const mockStore = configureStore();

describe('ModalText', () => {
    test('renders ModalText', () => {
        const initialState = {
          main: {
            isOpenModalText: true,
            currentCard: {
                src: "test-image.jpg",
                title: "Test Product",
                price: 10,
              },
          },
        };
        const store = mockStore(initialState);
        render(
            <Provider store={store}>  
                <ModalText/>
            </Provider>);
        expect(screen.getByText('Бажаєте видалити цей товар?')).toBeInTheDocument();
    });

    test('works button Yes', () => {
      const initialState = {
        main: {
          isOpenModalText: true,
          currentCard: {
              src: "test-image.jpg",
              title: "Test Product",
              price: 10,
            },
        },
      };
      const store = mockStore(initialState);
      render(
          <Provider store={store}>  
              <ModalText/>
          </Provider>);

      fireEvent.click(screen.getByText('Так'));
      const actions = store.getActions();
      expect(actions).toEqual([
      {
          type: toggleCartProduct.type,
          payload: { currentCard: {
              src: "test-image.jpg",
              title: "Test Product",
              price: 10,
            }, action: 'remove' },
      },
      { type: toggleModal.type, payload: 'modalText' },
      ]);
    });

    test('works button No', () => {
      const initialState = {
        main: {
          isOpenModalText: true,
          currentCard: {
              src: "test-image.jpg",
              title: "Test Product",
              price: 10,
            },
        },
      };
      const store = mockStore(initialState);
      render(
          <Provider store={store}>  
              <ModalText/>
          </Provider>);
      fireEvent.click(screen.getByText('Ні'));
        const secondActions = store.getActions();
        expect(secondActions).toEqual([
        { type: toggleModal.type, payload: 'modalText' },
        ]);
    });
})
