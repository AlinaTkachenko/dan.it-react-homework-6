import Button from "../../Button/Button";
import PropTypes from 'prop-types';

const ModalFooter = ({firstText, secondaryText, firstClick, secondaryClick}) => {

    return (
        <div className='modal__footer'>
            {firstText && <Button onClick={firstClick}>{firstText}</Button>}
            {secondaryText && <Button onClick={secondaryClick}>{secondaryText}</Button>}  
        </div>
    )
}

ModalFooter.propTypes = {
    firstText: PropTypes.string,
    secondaryText: PropTypes.string,
    firstClick: PropTypes.func,
    secondaryClick: PropTypes.func
  };

export default ModalFooter;