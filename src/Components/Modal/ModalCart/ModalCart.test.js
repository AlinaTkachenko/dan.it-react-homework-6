import { render, screen, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import store from "../../../store";
import configureStore from "redux-mock-store";
import ModalCart  from "./ModalCart";
import { toggleCartProduct, toggleModal } from "../../../store/mainSlice";

const mockStore = configureStore();

describe('ModalCart', () => {
    test('renders modalCart', () => {
        const modalCart = render(
            <Provider store={store}>   
                <ModalCart/>                            
            </Provider>
          )
        expect(modalCart).toMatchSnapshot();
    });

    test("renders with correct content", () => {
        const initialState = {
          main: {
            isOpenModalCart: true,
            currentCard: {
              src: "test-image.jpg",
              title: "Test Product",
              price: 10,
            },
          },
        };
    
        const store = mockStore(initialState);
    
        render(
          <Provider store={store}>
            <ModalCart />
          </Provider>
        );
    
        // Проверка наличия элементов в модальном окне
        expect(screen.getByAltText("Test Product")).toBeInTheDocument();
        expect(screen.getByText("Test Product")).toBeInTheDocument();
        expect(screen.getByText("10 грн.")).toBeInTheDocument();
        expect(screen.getByText("Додати до кошика")).toBeInTheDocument();
      });

      test("closes modal when clicking 'Додати до кошика'", () => {
        const initialState = {
          main: {
            isOpenModalCart: true,
            currentCard: {
              src: "test-image.jpg",
              title: "Test Product",
              price: 10,
            },
          },
        };
    
        const store = mockStore(initialState);
    
        render(
          <Provider store={store}>
            <ModalCart />
          </Provider>
        );
    
        // Клик по кнопке 'Додати до кошика'
        fireEvent.click(screen.getByText("Додати до кошика"));
    
        // Проверка, что экшн toggleCartProduct вызван и модальное окно закрыто
        const actions = store.getActions();
        expect(actions).toEqual([
          {
            type: toggleCartProduct.type,
            payload: { currentCard: {
                src: "test-image.jpg",
                title: "Test Product",
                price: 10,
              }, action: "plus" },
          },
          { type: toggleModal.type, payload: "modalCart" },
        ]);
      });
})
