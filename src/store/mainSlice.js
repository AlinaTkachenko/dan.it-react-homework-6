import { createSlice } from "@reduxjs/toolkit";

const mainSlice = createSlice({
    name: "main",
    initialState: {
        products: [],
        favorites: [],
        cartProducts: [],
        currentCard: {},
        isOpenModalCart: false,
        isOpenModalText: false,
    },
    reducers: {
        addPoducts(state, action) {
            state.products = [...action.payload]
        },
        setCurrentCard(state, action) {
            state.currentCard = action.payload
        },
        toggleModal(state, action) {
            if(action.payload === "modalCart") {
                state.isOpenModalCart = !state.isOpenModalCart
            }
            if(action.payload === "modalText") {
                state.isOpenModalText = !state.isOpenModalText
            }
        },
        toggleFavorites(state, action) {
            if (!state.favorites.find(favorite => favorite.id === action.payload.id)) {
                state.favorites.push(action.payload)
            } else {
                state.favorites = state.favorites.filter(item => action.payload.id !== item.id);
            }
        },
        toggleCartProduct(state, action) {

            switch (action.payload.action) {
                case 'plus':

                    if(!state.cartProducts.find(item => item.id === action.payload.currentCard.id)) {
                        state.cartProducts.push({...action.payload.currentCard, count: 1});
                    }
                    else {
                        state.cartProducts.map(item => {    
                            if(item.id === action.payload.currentCard.id) {
                                item.count++
                            }
                        }) 
                    }  
                    break;
                case 'minus':

                    state.cartProducts.map(item => {    
                        if(item.id === action.payload.currentCard.id) {
                            item.count--
                        }
                    }) 
                    break;
                case 'remove':

                    state.cartProducts = state.cartProducts.filter(item => item.title !== action.payload.currentCard.title)
                  break;
            }
        },
        removeCartProducts(state, action) {
            state.cartProducts = [];
        }           
    }      
});

export const {
    addPoducts,
    setCurrentCard,
    toggleModal,
    toggleFavorites,
    toggleCartProduct,
    removeCartProducts
} = mainSlice.actions;

export const fetchProducts = (category) => (dispatch) => {
    fetch(`/data.json`)
        .then(response => {
            return response.json();    
        })
        .then(data => {
            return dispatch(addPoducts(data[category]))
        })
}

export default mainSlice.reducer;