import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { setCurrentCard, toggleModal } from "../../../store/mainSlice";

import ColorProductCard from "../../../Components/ProductCard/ComponentsProductCard/ColorProductCard";
import ImageProductCard from "../../../Components/ProductCard/ComponentsProductCard/ImageProductCard";
import PriceProductCard from "../../../Components/ProductCard/ComponentsProductCard/PriceProductCard";
import TitleProductCard from "../../../Components/ProductCard/ComponentsProductCard/TitleProductCard";
import Counter from "../../../Components/Counter/Counter";
import Button from "../../../Components/Button/Button";

const CartProductCard = ({product}) => { 
    const{src, title, color, price} = product;
    const dispatch = useDispatch();
    
    return (
        <div className="cart__product-card row">
            <ImageProductCard src={src} optionalClassNames="cart__image"/> 
            <div className="cart__wrapper">
                <div>
                    <TitleProductCard optionalClassNames="cart__title">{title}</TitleProductCard>
                    <ColorProductCard color={color}/>
                    <PriceProductCard price={price}/>
                </div>
                <Counter product={product} />
                <Button optionalClassNames="cart__button" 
                    onClick={()=>{
                        dispatch(toggleModal("modalText"));
                        dispatch(setCurrentCard(product));}
                    }>Видалити</Button>
            </div>       
        </div>
    )
}

CartProductCard.propTypes = {
    product: PropTypes.object,
  };

export default CartProductCard;