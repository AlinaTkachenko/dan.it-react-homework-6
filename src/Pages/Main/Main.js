import "./Main.scss";

import Title from "../../Components/Title/Title";

const Main = () => {

    return (
        <div className="main">
            <Title>Головна</Title>
        </div>
    )
}

export default Main;