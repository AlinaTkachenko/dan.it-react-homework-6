import { useEffect, useContext } from "react";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { fetchProducts } from "../../store/mainSlice";

import "./Catalog.scss";

import Products from "../../Components/Products/Products";
import { ReactComponent as DataTable} from "../../Components/Svg/data_table.svg";
import { ReactComponent as Cards} from "../../Components/Svg/cards.svg";
import { ProductsViewContext } from "../../Components/AppRoutes/AppRoutes";

const Catalog = ({toggleProductsView}) => {
    const products = useSelector(state => state.main.products); 
    const dispatch = useDispatch();
    const { category } = useParams(); 

    const productsView = useContext(ProductsViewContext);
    
    useEffect(() => { 
        dispatch(fetchProducts(category))
    }, [category]) 

    return (
            <div className="catalog">
                <div className="catalog__switcher"> 
                    {productsView?<DataTable onClick={toggleProductsView}/>:<Cards onClick={toggleProductsView}/>} 
                </div>
                <Products 
                    category={category}
                    products={products}
                />
            </div>
    )
}

export default Catalog;