import { render, screen } from '@testing-library/react';
import { Provider } from "react-redux";
import store, { persistor } from "./store";
import { PersistGate } from 'redux-persist/integration/react'
import { BrowserRouter } from "react-router-dom";
import App from './App';


describe('App', () => {

  test('renders header and footer', () => {
    render(
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <BrowserRouter>
            <App />
          </BrowserRouter>
        </PersistGate>
      </Provider>
    )
    const logo = screen.getAllByText(/Electronic Bike/);
    expect(logo.length).toBe(2);
  });

  test('renders menu in header', () => {
    render(
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <BrowserRouter>
            <App />
          </BrowserRouter>
        </PersistGate>
      </Provider>
    )
    const itemMenu = screen.getByText(/Контакти/);
    expect(itemMenu).toBeInTheDocument();
  })
  
});

